import { writable, derived, get } from "svelte/store";
import { SELECT_PERSON, PERSON_TYPES, PERSONS, TEXT, TYPING, LOADING, SHARING } from "./constants";
import {save, restore} from 'helpers/carret'
export const view = (()=>{
  const {set, subscribe} = writable(SELECT_PERSON);
  return {
    subscribe,
    set,
    text: ()=>set(TEXT),
    selectPerson: ()=>set(SELECT_PERSON)
  }
})()
// export const view = writable(TEXT);

export const personIndex = (() => {
  const { subscribe, set } = writable(0);
  return { subscribe, set, reset: () => set(0) };
})();

export const personType = (() => {
  const { subscribe, set } = writable(PERSON_TYPES[1]);
  return {
    subscribe,
    select: (type) => {
      set(type);
      personIndex.reset();
    },
  };
})();

export const person = derived(
  [personIndex, personType],
  ([$personIndex, $personType]) => PERSONS[$personType][$personIndex]
);

export const contentNode = (() => {
  const store = writable(null);
  let sel 
  const saveCarretPosition = () => {
    sel = save(get(store))
  
  };
  return {
    subscribe: store.subscribe,
    set: (el) => {
      if(el){
        el.focus()
        el.addEventListener("keyup", saveCarretPosition);
        el.addEventListener("mouseup", saveCarretPosition);
      } 
      store.set(el);
      
      
    },
    insertImage: (url) => {
      restore(get(store), sel)
      document.execCommand("insertImage", false, url);
      document.execCommand("insertText",false, ' ')
      saveCarretPosition()
    },
  };
})();

export const isStickersOpen = (() => {
  const { subscribe, set } = writable(false);
  return { subscribe, open: () => set(true), close: () => set(false) };
})();

export const innerHeight = writable(window.innerHeight)

export const textState = writable(TYPING)
// export const textState = writable(SHARING)

export const sendText = ()=>{
  textState.set(LOADING)
}

export const showShare = ()=>{
  textState.set(SHARING)
}

export const closeShare = ()=>{
  textState.set(TYPING)
}

