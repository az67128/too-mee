// VIEWS
export const SELECT_PERSON = "SELECT_PERSON";
export const TEXT = "TEXT";

// PERSON TYPES
export const CARTOONS = "Мультфильмы";
export const ACTORS = "Актёры";
export const PERSON_TYPES = [CARTOONS, ACTORS];

// TEXT STATE
export const TYPING = 'TYPING'
export const LOADING = 'LOADING'
export const SHARING = 'SHARING'


export const PERSONS = {
  [CARTOONS]: [
    { id: "alisa", image: "alisa.png", name: "Алиса Селезнева" },
    { id: "gromozeka", image: "gromozeka.png", name: "Громозека" },
    { id: "masha", image: "masha.png", name: "Маша" },
  ],
  [ACTORS]: [
    
    {
      id: "kutsenko",
      apiId:'KUC', 
      image: "kutsenko.png",
      name: "гоша куценко",
      emojiList: [
        { image: "smile.png", id: "smile", emoji: "😀" },
        { image: "hearts.png", id: "hearts", emoji: "🥰" },
        { image: "partying.png", id: "partying", emoji: "🥳" },
        { image: "screaming.png", id: "screaming", emoji: "😱" },
        { image: "kiss.png", id: "kiss", emoji: "😘" },
        { image: "crying.png", id: "crying", emoji: "😭" },
      ],
    },
    { id: "dud", image: "dud.png", name: "Юрий Дудь" },
  ],
};
